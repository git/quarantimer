module Config where

hostname :: String
hostname = "quarantimer.app"

staticDir :: FilePath
staticDir = "static"

maxImageSize :: Int
maxImageSize = 640

numImageCores :: Int
numImageCores = 1

-- Port that the server also listens for http traffic on.
-- This allows using the server even when it can't bind to low ports.
devPort :: Int
devPort = 7070
