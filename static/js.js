var things;
var streaming=false;
var imageheight;
var video;
var canvas;
var photo;
var formphoto;
var capturebutton;
var uploadbutton;

window.onload=function(){
	var loaddate = Date.now();
	things = document.getElementsByClassName('thing');
	for (var i = 0; i < things.length; i++) {
		var elt = things[i];
		var img = elt.getElementsByTagName("img")[0];
		things[i].img = img;
		var timer = elt.getElementsByClassName("timer")[0];
		things[i].timer = timer;
		if (timer != null) {
			var n = parseInt(timer.attributes.title.value);
			var d = loaddate + (n * 1000);
			things[i].enddate = d;
		}
	}

	animate();
	checkSessionId();

	video = document.getElementById('video');
	canvas = document.getElementById('canvas');
	photo = document.getElementById('photo');
	formphoto = document.getElementById('formphoto');
	capturebutton = document.getElementById('capturebutton');
	uploadbutton = document.getElementById('uploadbutton');

	capturebutton.addEventListener('click', function(ev) {
		togglevideo();
		ev.preventDefault();
	}, false);
	uploadbutton.addEventListener('click', function(ev) {
		takephoto();
	}, false);
	video.addEventListener('canplay', function(ev){
		streaming = true;
		imageheight = video.videoHeight / (video.videoWidth/imagesize);
		video.setAttribute('width', imagesize);
		canvas.setAttribute('width', imagesize);
		canvas.setAttribute('height', imageheight);
		photo.setAttribute('width', imagesize);
		photo.setAttribute('height', imageheight);
	}, false);
}

function togglevideo() {
	if (streaming) {
		takephoto();
	} else {
		photo.style.display = "none";
		video.style.display = "block";
		var constraints = { video: { facingMode: "environment" }, audio: false };
		navigator.mediaDevices.getUserMedia(constraints)
			.then(function(stream) {
				video.srcObject = stream;
				video.play();
			})
			.catch(function(err) {
				console.log("An error occurred: " + err);
			});
	}
}

function takephoto() {
	if (streaming) {
		var context = canvas.getContext('2d');
		context.drawImage(video, 0, 0, imagesize, imageheight);
		var data = canvas.toDataURL('image/png');
		photo.setAttribute('src', data);
		formphoto.value = data;
		video.style.display = "none";
		photo.style.display = "block";
	
		video.srcObject.getTracks().forEach(function(track) {
			track.stop();
		});
		video.srcObject = null;
		streaming = false;
	}
}

function displayUnit(v,u) {
	var s;
	if (v == 1) { s = "" } else { s = "s" }
	return (v + " " + u + s);
}

function animate() {
	now = Date.now();
	for (var i = 0; i < things.length; i++) {
		var img = things[i].img;
		var timer = things[i].timer;
		if (things[i].enddate != null) {
			var diff = (things[i].enddate - now) / 1e3;
			var h=(diff/60/60)>>0;
			var m=(diff/60 - h*60)>>0;
			if (diff > 0) {
				img.className = "infected";
				timer.innerHTML = "quarantine ends in " +
					displayUnit(h,"hour") + " " +
					displayUnit(m,"minute")
			} else {
				img.className = "safe";
				timer.innerHTML = "quarantine completed " +
					displayUnit((h*-1),"hour") + " ago"
			}
		} else {
			img.className = "infected";
		}
	}

	setTimeout(animate,10000); // keep updating every 10s
}

function checkSessionId() {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			if (JSON.parse(this.responseText) != sessionid) {
				location.reload(true);
			}
		}
	};
	xhttp.open("GET", "/sessionid", true);
	xhttp.send();
	setTimeout(checkSessionId, 3600000); // check every hour
}

function validateform() {
	var surfaces=document.getElementsByClassName("surface");
	var sok=false;
	for (var i=0,l=surfaces.length;i<l;i++) {
		if (surfaces[i].checked) {
			sok=true;
		}
	}
	if (formphoto.value.length == 0) {
		alert("Please take a photo of the thing you want to quarantine.");
		event.preventDefault();
	}
	else {
		if (! sok) {
			alert("Please indicate what the surface of the item is made of.");
			event.preventDefault();
		}
		else {
			document.getElementById('uploadmsg').style.display = 'block';
			document.getElementById('uploadover').style.display = 'block';
		}
	}
}
